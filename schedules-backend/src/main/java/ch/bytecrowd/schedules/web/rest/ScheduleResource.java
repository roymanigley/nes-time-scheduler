package ch.bytecrowd.schedules.web.rest;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.bytecrowd.schedules.domain.dto.ScheduleDTO;
import ch.bytecrowd.schedules.service.ScheduleService;

@RestController
@RequestMapping(path = "/api/schedules")
@CrossOrigin(allowedHeaders = "*")
public class ScheduleResource {
    
    private final ScheduleService service;

    public ScheduleResource(ScheduleService service) {
        this.service = service;
    }

    @GetMapping
    public Map<String, List<Entry<LocalDate, List<ScheduleDTO>>>> getInDateRangeGrouped(
        @DateTimeFormat(iso = ISO.DATE) @RequestParam(name = "startDate", required = false) LocalDate startDate, 
        @DateTimeFormat(iso = ISO.DATE) @RequestParam(name = "endDate", required = false) LocalDate endDate) {
        return service.findInDateRangeGrouped(startDate, endDate);
    }
    
    @GetMapping(path = "/{id}")
    public Optional<ScheduleDTO> getById(@PathVariable Long id) {
        return service.findById(id);
    }
    
    @PostMapping
    public ScheduleDTO save(@RequestBody ScheduleDTO schedule) {
        return service.save(schedule);
    }
    
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}