package ch.bytecrowd.schedules.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;

import ch.bytecrowd.schedules.domain.dto.ScheduleDTO;

/**
 * ScheduleService
 */
public interface ScheduleService {

    List<ScheduleDTO> findAllSchedules();
    Map<String, List<Entry<LocalDate, List<ScheduleDTO>>>> findInDateRangeGrouped(LocalDate startDate, LocalDate endDate);
    Optional<ScheduleDTO> findById(Long scheduleId);
    ScheduleDTO save(ScheduleDTO schedule);
    void delete(Long scheduleId);
    
}