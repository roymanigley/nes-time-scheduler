package ch.bytecrowd.schedules.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import ch.bytecrowd.schedules.domain.dto.ScheduleDTO;
import ch.bytecrowd.schedules.domain.entities.Schedule;
import ch.bytecrowd.schedules.repository.ScheduleRepository;
import ch.bytecrowd.schedules.service.ScheduleService;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduleRepository repository;

    public ScheduleServiceImpl(ScheduleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ScheduleDTO> findAllSchedules() {
        return repository.findAll().stream().map(ScheduleDTO::from).collect(Collectors.toList());
    }

    private static final DateTimeFormatter MONTH_FORMAT = DateTimeFormatter.ofPattern("MMMM");
    
    @Override
    public Map<String, List<Entry<LocalDate, List<ScheduleDTO>>>> findInDateRangeGrouped(LocalDate startDate, LocalDate endDate) {
        return repository.findByRange(            
                Optional.ofNullable(startDate).orElseGet(() -> LocalDate.now().minusWeeks(1)), 
                Optional.ofNullable(endDate).orElseGet(LocalDate::now)
            ).stream()
            .collect(Collectors.groupingBy(
                Schedule::getStartDate,
                LinkedHashMap::new, 
                Collectors.mapping(schedule -> ScheduleDTO.from(schedule), Collectors.toList())
                )
            ).entrySet().stream()
            .collect(Collectors.groupingBy(
                e -> MONTH_FORMAT.format(e.getKey()), 
                LinkedHashMap::new, 
                Collectors.toList())
            );    
    }

    @Override
    public Optional<ScheduleDTO> findById(Long scheduleId) {
        return repository.findById(scheduleId).map(ScheduleDTO::from);
    }

    @Transactional
    @Override
    public ScheduleDTO save(ScheduleDTO schedule) {
        return ScheduleDTO.from( 
            repository.save(schedule.toEntity())
        ); 
    }

    @Transactional
    @Override
    public void delete(Long scheduleId) {
        repository.deleteById(scheduleId);
    }
    

}