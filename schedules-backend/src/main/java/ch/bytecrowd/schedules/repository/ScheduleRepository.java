package ch.bytecrowd.schedules.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.bytecrowd.schedules.domain.entities.Schedule;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    
    @Query("from Schedule s where s.startDate >= :startDate and s.startDate <= :endDate order by s.startDate desc")
    List<Schedule> findByRange(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);
}