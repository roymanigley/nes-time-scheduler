package ch.bytecrowd.schedules.domain.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;

import ch.bytecrowd.schedules.domain.entities.Schedule;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ScheduleDTO {
    
    private Long id;
    
    private String title;
    
    private String description;
    
    private LocalDate startDate;
    
    private String startTime;
    
    private LocalDate endDate;
    
    private String endTime;

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    

    public Schedule toEntity() {
        return Schedule.builder()
            .id(this.getId())
            .title(this.getTitle())
            .description(this.getDescription())
            .startDate(this.getStartDate())
            .startTime(ScheduleDTO.mapLocalTime(this.getStartTime()))
            .endDate(this.getEndDate())
            .endTime(ScheduleDTO.mapLocalTime(this.getEndTime()))
            .build();
    }

    public static ScheduleDTO from(Schedule schedule) {
        return ScheduleDTO.builder()
            .id(schedule.getId())
            .title(schedule.getTitle())
            .description(schedule.getDescription())
            .startDate(schedule.getStartDate())
            .startTime(ScheduleDTO.mapTimeString(schedule.getStartTime()))
            .endDate(schedule.getEndDate())
            .endTime(ScheduleDTO.mapTimeString(schedule.getEndTime()))
            .build();
    }

    private static String mapTimeString(LocalTime time) {
        return Optional.ofNullable(time)
            .map(t -> TIME_FORMATTER.format(t))
            .orElse(null);
    }

    private static LocalTime mapLocalTime(String time) {
        return Optional.ofNullable(time)
            .map(t -> LocalTime.from(TIME_FORMATTER.parse(t)))
            .orElse(null);
    }
}