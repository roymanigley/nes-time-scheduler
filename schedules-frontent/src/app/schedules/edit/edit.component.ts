import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Schedule } from 'src/app/domain/schedule';
import { ScheduleService } from 'src/app/service/schedule.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  timePattern = "[0-9]{2}:[0-9]{2}"
  
  selected: Schedule;
  editStartDate: boolean;
  editEndDate: boolean;

  constructor(private route: ActivatedRoute, private service: ScheduleService, private router: Router) { }

  ngOnInit(): void {
    this.selected = this.route.snapshot.data["selected"];
    this.editStartDate = this.selected.startDate == null
    this.editEndDate = this.selected.endDate == null
  }

  onSubmit() {
    this.service.save(this.selected).subscribe(schedule =>{
      this.selected = schedule;
      this.navigateBack();
    });
  }

  saveAndClose() {
    this.selected.endDate = new Date()
    this.selected.endTime = 
      (this.selected.endDate.getHours() < 10 ? "0" : "") + this.selected.endDate.getHours() 
      + ":" + 
      (this.selected.endDate.getMinutes() < 10 ? "0" : "") + this.selected.endDate.getMinutes()
    this.onSubmit()
  }

  onDelete() {
    this.service.delete(this.selected).subscribe(() => {
      this.navigateBack()
    })
  }

  onDateTimeBlur(): void {
    if (this.selected.startTime?.length >= 3 && this.selected.startTime?.length <= 4 && !this.selected.startTime.includes(":")) {
      this.selected.startTime = 
        (this.selected.startTime.length == 3 ? "0" : "") +
        this.selected.startTime.substring(0, this.selected.startTime.length / 2) + ":"
        + this.selected.startTime.substring(this.selected.startTime.length / 2, this.selected.startTime.length) 
    }
    if (this.selected.endTime?.length >= 3 && this.selected.endTime?.length <= 4 && !this.selected.endTime.includes(":")) {
      this.selected.endTime = 
        (this.selected.endTime.length == 3 ? "0" : "") +
        this.selected.endTime.substring(0, this.selected.endTime.length / 2) + ":" + this.selected.endTime.substring(this.selected.endTime.length / 2, this.selected.endTime.length) 
    }
  }

  navigateBack(): void {
    this.router.navigate(["schedules-list"])
  }
}
