import { Injectable } from '@angular/core';
import { Schedule } from '../domain/schedule';
import { Observable, of } from 'rxjs';
import { ScheduleService } from '../service/schedule.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ScheduleResolver implements Resolve<Map<String, Map<Date, Schedule[]>>> {

  constructor(private service: ScheduleService) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Map<String, Map<Date, Schedule[]>>> {
    let startDate = route.params["startDate"]
    let endDate = route.params["endDate"]
    return this.service.getRecords(startDate, endDate)
  }
}

@Injectable({
  providedIn: 'root'
})
export class ScheduleToDayResolver implements Resolve<Schedule[]> {

  constructor(private service: ScheduleService) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Schedule[]> {
    return this.service.getRecordsToDay()
  }
}

@Injectable({
  providedIn: 'root'
})
export class ScheduleCreateResolver implements Resolve<Schedule> {

  constructor() { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Schedule> {
    const now = new Date()
    const startTime = (now.getHours() < 10 ? "0" : "") + now.getHours() + ":" + (now.getMinutes() < 10 ? "0" : "") + now.getMinutes();
    return of(new Schedule(null, null, null, now, null, startTime))
  }
  
}

@Injectable({
  providedIn: 'root'
})
export class ScheduleUpdateResolver implements Resolve<Schedule> {

  constructor(private service: ScheduleService) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Schedule> {
    const id:number = route.params["id"];
    return this.service.getRecordById(id)
  }
  
}
