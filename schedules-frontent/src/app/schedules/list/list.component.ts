import { Component, OnInit } from '@angular/core';
import { ScheduleService } from 'src/app/service/schedule.service';
import { scheduled } from 'rxjs';
import { Schedule } from 'src/app/domain/schedule';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  
  months: string[]
  editStartDate: boolean = false;
  editEndDate: boolean = false;
  filterMonth: string

  startDate: Date = new Date()
  endDate: Date = new Date()
  schedules: Map<String, Map<Date, Schedule[]>>;

  constructor(private route: ActivatedRoute, private router: Router, private service: ScheduleService) { }

  ngOnInit(): void {
    this.initData(
      <Map<String, Map<Date, Schedule[]>>> this.route.snapshot.data["schedules"]
    ) 
  }

  onDateChange() {
    if (this.startDate) {
      this.router.navigate(["/schedules-list", this.startDate, this.endDate])
      setTimeout(() => { this.ngOnInit() }, 200)
    }
  }

  private initData(data: Map<String, Map<Date, Schedule[]>>) {
    this.editStartDate = false
    this.editEndDate = false
    
    this.schedules = data;
    this.months = this.getKeys(this.schedules);
    const lastMonth = this.months.length - 1;
    if (lastMonth >= 0) {
      const lastDay = this.schedules[this.months[lastMonth]].length - 1;
      this.startDate = new Date(Date.parse(this.getKeys(this.schedules[this.months[lastMonth]][lastDay])[0]))
      this.endDate = new Date(Date.parse(this.getKeys(this.schedules[this.months[0]][0])[0]))
    }
  }

  sumTime(schedules: Schedule[]): number {
    let sum = 0;
    schedules.forEach((schedule) => {
      sum += Schedule.calcHoursDecimal(schedule)
    })
    return sum;
  }

  getKeys(obect: any): string[] {
    return Object.keys(obect);
  }

  calcHoursDecimal(schedule: Schedule): number {

    if (schedule.startTime?.length == 5 && schedule.endTime?.length == 5) {
        const startTimeSplitted = schedule.startTime.split(":")
        const endTimeSplitted = schedule.endTime.split(":")

        let hours = parseInt(endTimeSplitted[0]) - parseInt(startTimeSplitted[0])

        const startMinute = parseInt(startTimeSplitted[1]) 
        const endMinute = parseInt(endTimeSplitted[1])

        const minutes = Math.round((endMinute - startMinute) / 60 * 4) / 4;
        hours += minutes
        return hours
    }

    return 0;
}
}
