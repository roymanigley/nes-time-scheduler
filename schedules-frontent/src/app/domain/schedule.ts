import { Category } from './category';

export class Schedule {
    
    constructor(
        public id?:number,
        public title?:string,
        public description?:string,
        public startDate?:Date,
        public endDate?:Date,
        public startTime?:String,
        public endTime?:String,
        public categories:Category[]=[]
    ) {}

    public calcHoursDecimal(): number {
        return Schedule.calcHoursDecimal(this)
    }

    

  public static calcHoursDecimal(schedule: Schedule): number {

        if (schedule.startTime?.length == 5 && schedule.endTime?.length == 5) {
            const startTimeSplitted = schedule.startTime.split(":")
            const endTimeSplitted = schedule.endTime.split(":")

            let hours = parseInt(endTimeSplitted[0]) - parseInt(startTimeSplitted[0])

            const startMinute = parseInt(startTimeSplitted[1]) 
            const endMinute = parseInt(endTimeSplitted[1])

            const minutes = Math.round((endMinute - startMinute) / 60 * 4) / 4;
            hours += minutes
            return hours
        }

        return 0;
    }
}
