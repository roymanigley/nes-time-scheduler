import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent as ScheduleListComponent } from './schedules/list/list.component';
import { ScheduleResolver, ScheduleToDayResolver, ScheduleCreateResolver, ScheduleUpdateResolver } from './schedules/schedule.resolver';
import { EditComponent as ScheduleEditComponent} from './schedules/edit/edit.component';

const routes: Routes = [
  {path: "schedules/create", component: ScheduleEditComponent, resolve: { selected: ScheduleCreateResolver }},
  {path: "schedules/edit/:id", component: ScheduleEditComponent, resolve: { selected: ScheduleUpdateResolver }},
  {path: "schedules-list/:startDate/:endDate", component: ScheduleListComponent, resolve: { schedules: ScheduleResolver }},
  {path: "schedules-list/:startDate", component: ScheduleListComponent, resolve: { schedules: ScheduleResolver }},
  {path: "schedules-list", component: ScheduleListComponent, resolve: { schedules: ScheduleResolver }},
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
