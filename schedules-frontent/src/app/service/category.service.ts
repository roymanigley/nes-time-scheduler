import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Category } from '../domain/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getRecords(): Observable<Category[]> {
    return of(
      [
        new Category(1, "Cat One"),
        new Category(2, "Cat Two"),
        new Category(3, "Cat Three"),
        new Category(4, "Cat Four"),
        new Category(5, "Cat Five"),
      ]
    )
  }
}
