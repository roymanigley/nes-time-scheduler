import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Schedule } from '../domain/schedule';
import { Observable, of, scheduled } from 'rxjs';

const apiBase = "http://localhost:8080/api"
@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }

  getRecords(startDate: string, endDate: string): Observable<Map<String, Map<Date, Schedule[]>>> {
    let params = ""
    if (startDate) {
      params = "?startDate=" + startDate
    if (endDate) 
        params += "&endDate=" + endDate
    }
    return this.http.get<Map<String, Map<Date, Schedule[]>>>(apiBase + "/schedules" + params)
  }

  getRecordsToDay(): Observable<Schedule[]> {
    return this.http.get<Schedule[]>(apiBase + "/schedules")
  }
  
  getRecordById(id: number): Observable<Schedule> {
    return this.http.get<Schedule>(apiBase + "/schedules/" + id)
  }
  
  save(selected: Schedule) : Observable<Schedule> {
    return this.http.post<Schedule>(apiBase + "/schedules", selected)
  }

  delete(selected: Schedule) : Observable<void> {
    return this.http.delete<void>(apiBase + "/schedules/" + selected.id)
  }
}
